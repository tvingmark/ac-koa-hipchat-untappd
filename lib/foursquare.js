var request = require('co-request');
var qs = require('querystring');
var env = require('./util').env;

var clientId = env('FOURSQUARE_CLIENT_ID');
var clientSecret = env('FOURSQUARE_CLIENT_SECRET');
var version = '20140930';

function get(path, params) {
  var baseUrl = 'https://api.foursquare.com/v2';
  return function *() {
    url = baseUrl + path;
    params = params || {};
    params.client_id = clientId,
    params.client_secret = clientSecret;
    params.v = version;
    url += '?' + qs.stringify(params);
    var response = yield request.get(url);
    if (response.body) {
      return JSON.parse(response.body);
    }
  };
};

exports.getVenues = function (address, query) {
  return function *() {
    return yield get('/venues/explore', {
      near: address,
      query: query
    });
  };
};
