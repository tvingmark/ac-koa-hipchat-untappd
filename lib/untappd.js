var request = require('co-request');
var wait = require('co-wait');
var qs = require('querystring');
var env = require('./util').env;
var foursquare = require('./foursquare');
var logger = require('winston');

var clientId = env('UNTAPPD_CLIENT_ID');
var clientSecret = env('UNTAPPD_CLIENT_SECRET');

function get(path, params) {
  var baseUrl = 'https://api.untappd.com/v4';
  return function *() {
    url = baseUrl + path;
    params = params || {};
    params.client_id = clientId,
    params.client_secret = clientSecret;
    if (Object.keys(params).length > 0) {
      url += '?' + qs.stringify(params);
    }
    var response = yield request.get(url);
    if (response.body) {
      return JSON.parse(response.body);
    }
  };
};

function mapVenue(fsVenueId) {
  return function *() {
    var results = yield get('/venue/foursquare_lookup/' + fsVenueId);
    if (results.response && results.response.venue && results.response.venue.items.length > 0) {
      return results.response.venue.items[0].venue_id;
    }
  };
}

function getVenue(id) {
  return function *() {
    return yield get('/venue/info/' + id);
  };
}

exports.findVenue = function (address, query, attempt) {
  attempt = attempt || 0;
  return function *() {
    var results = yield foursquare.getVenues(address, query);
    if (results.meta.code === 500) {
      if (attempt < 5) {
        logger.warn('FourSquare venue search API error, trying again in 1s...');
        yield wait(1000);
        return yield exports.findVenue(address, query, attempt + 1);
      } else {
        throw new Error('FourSquare is being stubborn. Please try again later.');
      }
    }
    var venues = results.response.groups && results.response.groups.find(function (group) {
      return group.name === 'recommended';
    });
    venues = venues && venues.items;
    if (venues && venues.length > 0) {
      var fsVenue = venues[0].venue;
      if (fsVenue) {
        var uVenueId = yield mapVenue(fsVenue.id);
        if (uVenueId) {
          var result = yield getVenue(uVenueId);
          var uVenue = result.response.venue;
          uVenue.location = fsVenue.location;
          return uVenue;
        }
      }
    }
  };
};
