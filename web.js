var track = require('ac-koa-hipchat-keenio').track;
var Notifier = require('ac-koa-hipchat-notifier').Notifier;
var Commands = require('./lib/commands');

var ack = require('ac-koa').require('hipchat');
var pkg = require('./package.json');
var app = ack(pkg, {store: 'MongoStore'});

var addon = app.addon()
  .hipchat()
  .allowRoom(true)
  .scopes('send_notification');

track(addon);

var notifier = Notifier({format: 'html', dir: __dirname + '/messages'});
var commands = Commands(notifier);

addon.webhook('room_message', /^\/(?:untappd|taps?)(?:\s+(.+?)\s*$)?/i, function *() {
  if (!this.match[1] || /^help\b/.test(this.match[1])) {
    return yield notifier.sendTemplate('help');
  }
  var matcher = new RegExp('^(' + Object.keys(commands).join('|') + ')\\s+(.+?)\\s*$');
  var match = matcher.exec(this.match[1]);
  if (match && match[1]) {
    return yield commands[match[1]]([].slice.call(match, 2));
  }
  yield notifier.send('Sorry, I didn\'t understand that.');
});

app.listen();
